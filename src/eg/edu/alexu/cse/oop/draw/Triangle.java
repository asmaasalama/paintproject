/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.edu.alexu.cse.oop.draw;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author asmaa
 */

public class Triangle implements Shape
{
    /*
    private final int []arrayX;
    private final int []arrayY;

    public Triangle(int[] arrayX, int[] arrayY) {
        this.arrayX = arrayX;
        this.arrayY = arrayY;
    }
    */
    
                    // Co-ordinates of the upper left corner of the shape
    private Point position;
    private Color color;
    private Color fillColor;
    private Map<String, Double> properties;
    private Integer index = new Integer(0);
    
    @Override
    public void setPosition(Point position) 
    {
        this.position = position;
    }
    
    @Override
    public Point getPosition() 
    {
        return position;
    }

    @Override
    public void setProperties(Map<String, Double> properties) 
    {
        this.properties = properties;
    }
    
    @Override
    public Map<String, Double> getProperties()
    {
        if(properties==null)
        {
            properties = new HashMap<String, Double>();
            //properties.put("x", 0.0);
            //properties.put("y", 0.0);
            properties.put("x2", 0.0);
            properties.put("y2", 0.0);
            properties.put("x3", 0.0);
            properties.put("y3", 0.0);
        }
        return properties;
    }

    @Override
    public void setColor(Color color) 
    {
        this.color = color;
    }

    @Override
    public Color getColor()
    {
        return color;     
    }

    @Override
    public void setFillColor(Color fillColor)
    {
        this.fillColor = fillColor;
    }

    @Override
    public Color getFillColor() 
    {
        return fillColor;
    }
    
    @Override
    public Object clone()
    {
        Triangle cloned = new Triangle();
        cloned.setColor(color);
        cloned.setFillColor(fillColor);
        Map<String, Double> clonedProperties = new HashMap<String, Double>();
        for (Map.Entry<String, Double> entry :properties.entrySet()) {
            clonedProperties.put(entry.getKey(), entry.getValue());
        }
        cloned.setProperties(clonedProperties);
        cloned.setPosition(position);
        return cloned;
    }
    
    @Override
    public void draw(Graphics canvas)
    {
        int[] arrayX = new int[3];
        int[] arrayY = new int[3];
        
        arrayX[0] = (int) getPosition().getX();
        arrayY[0] = (int) getPosition().getY();
        
        arrayX[1] = getProperties().get("x2").intValue();
        arrayY[1] = getProperties().get("y2").intValue();
        
        arrayX[2] = getProperties().get("x3").intValue();
        arrayY[2] = getProperties().get("y3").intValue();
        
        canvas.setColor(getColor());
        canvas.drawPolygon(arrayX, arrayY, 3);
        canvas.setColor(getFillColor());
        canvas.fillPolygon(arrayX, arrayY, 3);
        
    }
 
}
