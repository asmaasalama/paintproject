/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.edu.alexu.cse.oop.draw;

/**
 *
 * @author asmaa
 */
import com.code.SizedStack;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.reflections.Reflections;

public class Drawing implements DrawingEngine {

    //private ArrayList<Shape> shapes = null;    
    private List<Shape> shapes = null;
    private List<Class<? extends eg.edu.alexu.cse.oop.draw.Shape>> supportedShapes = null;
    private SizedStack<String> undoActionsStack = null;
    private SizedStack<String> redoActionsStack = null;
    private SizedStack<Shape> removedShapes = null;

    public Drawing() {
        shapes = new ArrayList<Shape>();
        undoActionsStack = new SizedStack<String>(20);
        redoActionsStack = new SizedStack<String>(20);
        removedShapes = new SizedStack<Shape>(20);

    }

    @Override
    public void addShape(eg.edu.alexu.cse.oop.draw.Shape shape) {
        undoActionsStack.push("addShape");
        shapes.add(shape);
    }

    @Override
    public void removeShape(eg.edu.alexu.cse.oop.draw.Shape shape) {
        undoActionsStack.push("removeShape");
        removedShapes.push(shape);

        shapes.remove(shape);

        
    }

    @Override
    public eg.edu.alexu.cse.oop.draw.Shape[] getShapes() {
        Shape[] currentShapes = new Shape[shapes.size()];
        return shapes.toArray(currentShapes);
    }

    @Override
    public void refresh(Graphics canvas) {
        for (Shape shape : shapes) {
            shape.draw(canvas);
        }
    }

    @Override
    public List<Class<? extends eg.edu.alexu.cse.oop.draw.Shape>> getSupportedShapes() {

        if (supportedShapes == null) {
            Reflections reflections = new Reflections("eg.edu.alexu.cse.oop.draw");
            Set<Class<? extends Shape>> supportedShapesSet = reflections.getSubTypesOf(Shape.class);
            supportedShapes = new ArrayList(supportedShapesSet);
        }
        return supportedShapes;
    }

    @Override
    public void installPluginShape(Class<? extends eg.edu.alexu.cse.oop.draw.Shape> shapeClass) {
        supportedShapes.add(shapeClass);
    }

    @Override
    public void undo() {
        String lastAction = !undoActionsStack.empty()?undoActionsStack.pop():null;
        if (lastAction != null) {
            if (lastAction.equals("addShape")) {
                removedShapes.push(shapes.get(shapes.size() - 1));
                shapes.remove(shapes.get(shapes.size() - 1));
                redoActionsStack.push("undoAdd");
            } else if (lastAction.equals("removeShape")) {
                shapes.add(removedShapes.pop());
                redoActionsStack.push("undoRemove");
            }
            
        }
    }

    @Override
    public void redo() {
        String lastAction = !redoActionsStack.empty()?redoActionsStack.pop():null;
        if (lastAction != null) {
            if (lastAction.equals("undoAdd")) {
                addShape(removedShapes.pop());
            } else if (lastAction.equals("undoRemove")) {
                removeShape(shapes.get(shapes.size() - 1));
            }
        }
    }
}