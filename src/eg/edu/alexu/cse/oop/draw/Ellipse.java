/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.edu.alexu.cse.oop.draw;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author asmaa
 */
public class Ellipse implements Shape
{
    
            // Co-ordinates of the upper left corner of the shape
    private Point position;
    private Color color;
    private Color fillColor;
    private Map<String, Double> properties;
    private Integer index = new Integer(0);
    
    @Override
    public void setPosition(Point position) 
    {
        this.position = position;
    }
    
    @Override
    public Point getPosition() 
    {
        return position;
    }

    @Override
    public void setProperties(Map<String, Double> properties) 
    {
        this.properties = properties;
    }
    
    @Override
    public Map<String, Double> getProperties()
    {
        if(properties==null)
        {
            properties = new HashMap<String, Double>();
            properties.put("width", 0.0);
            properties.put("height", 0.0);
        }
        return properties;
    }

    @Override
    public void setColor(Color color) 
    {
        this.color = color;
    }

    @Override
    public Color getColor()
    {
        return color;     
    }

    @Override
    public void setFillColor(Color fillColor)
    {
        this.fillColor = fillColor;
    }

    @Override
    public Color getFillColor() 
    {
        return fillColor;
    }
    
    @Override
    public Object clone()
    {
        Ellipse cloned = new Ellipse();
        cloned.setColor(color);
        cloned.setFillColor(fillColor);
        Map<String, Double> clonedProperties = new HashMap<String, Double>();
        for (Map.Entry<String, Double> entry :properties.entrySet()) {
            clonedProperties.put(entry.getKey(), entry.getValue());
        }
        cloned.setProperties(clonedProperties);
        cloned.setPosition(position);
        return cloned;
    }

    @Override
    public void draw(Graphics canvas) 
    {
        canvas.setColor(getColor());
        canvas.drawArc(getPosition().x, getPosition().y, getProperties().get("width").intValue(), getProperties().get("height").intValue(), 0, 360);
        canvas.setColor(getFillColor());
        canvas.fillArc(getPosition().x, getPosition().y, getProperties().get("width").intValue(), getProperties().get("height").intValue(), 0, 360);
    }
    
}
