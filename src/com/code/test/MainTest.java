/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.code.test;

import eg.edu.alexu.cse.oop.draw.Drawing;
import eg.edu.alexu.cse.oop.draw.Shape;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author asmaa
 */
public class MainTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) 
    {
        
        Drawing d = new Drawing();
        List<Class<? extends Shape>> supportedShapes = d.getSupportedShapes();
        
                
                //Reflections reflections = new Reflections("com.mycompany");
                //Set<Class<? extends Shape>> classes = reflections.getSubTypesOf(Shape.class);
;

        
        Frame drawingArea = new Frame();
        drawingArea.addWindowListener(new WindowAdapter(){
        public void windowClosing(WindowEvent we)
        {
            System.exit(0);
        }
        });
        
        drawingArea.setSize(300, 250);
        drawingArea.setVisible(true);
        
        // Test Circle
        /*
        Circle c = new Circle();
        Map<String, Double> properties = new HashMap<String, Double>();
        properties.put("radius", 10.0);
        c.setProperties(properties);
        Point p = new Point();
        p.setLocation(40, 40);
        c.setPosition(p);
        c.setColor(Color.DARK_GRAY);
        c.setFillColor(Color.RED);
        
        c.draw(drawingArea.getGraphics());
        */
        
        // Test Ellipse
        /*
        Ellipse e = new Ellipse();
        Map<String, Double> properties = new HashMap<String, Double>();
        properties.put("width", 20.0);
        properties.put("height", 10.0);
        e.setProperties(properties);
        Point p = new Point();
        p.setLocation(40, 40);
        e.setPosition(p);
        e.setColor(Color.DARK_GRAY);
        e.setFillColor(Color.BLUE); 
        
        e.draw(drawingArea.getGraphics());
        */
        
        // Test Line
        /*
        LineSegment l = new LineSegment();
        Map<String, Double> properties = new HashMap<String, Double>();
        properties.put("x2", 20.0);
        properties.put("y2", 10.0);
        l.setProperties(properties);
        Point p = new Point();
        p.setLocation(40, 40);
        l.setPosition(p);
        l.setColor(Color.BLUE);
        
        l.draw(drawingArea.getGraphics());
        */
        
        // Test Triangle
        
        
        /*
        Map<String, Double> properties = new HashMap<String, Double>();
        properties.put("x1", 40.0);
        properties.put("y1", 40.0);
        
        properties.put("x2", 20.0);
        properties.put("y2", 20.0);
        
        properties.put("x3", 20.0);
        properties.put("y3", 40.0);
        
        Triangle t = new Triangle();
        
        t.setProperties(properties);
        Point p = new Point();
        p.setLocation(40, 40);
        t.setPosition(p);
        t.setColor(Color.DARK_GRAY);
        t.setFillColor(Color.RED);
        
        t.draw(drawingArea.getGraphics());
        
        */
        // Test Square
        /*
        Square s = new Square();
        Map<String, Double> properties = new HashMap<String, Double>();
        properties.put("length", 30.0);
        s.setProperties(properties);
        Point p = new Point();
        p.setLocation(40, 40);
        s.setPosition(p);
        s.setColor(Color.DARK_GRAY);
        s.setFillColor(Color.RED); // the same problem
        
        s.draw(drawingArea.getGraphics());
        */
        
        // Test Rectangle
        /*
        Rectangle r = new Rectangle();
        Map<String, Double> properties = new HashMap<String, Double>();
        properties.put("width", 40.0);
        properties.put("length", 20.0);
        r.setProperties(properties);
        Point p = new Point();
        p.setLocation(40, 40);
        r.setPosition(p);
        r.setColor(Color.DARK_GRAY);
        r.setFillColor(Color.RED); // the same problem
        
        r.draw(drawingArea.getGraphics());
        */
    }
}
